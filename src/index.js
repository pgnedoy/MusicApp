import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import MyApp from './MyApp';
import registerServiceWorker from './registerServiceWorker';
// import { CookiesProvider } from "react-cookie";
import { Provider } from 'react-redux';
// import { Router, Route, browserHistory } from 'react-router-redux'
// import { createBrowserHistory } from 'history';
import { store, sagaMiddleware } from './store'
import SignUp from './components/authorization/SignUp';
import Login from './components/authorization/Login';
import UserProfile from './components/userMenu/UserProfile';
// import  MyApp  from "./MyApp";
import { BrowserRouter, Route, Switch } from "react-router-dom";



// import { BrowserRouter, Route } from "react-router-dom";

import {rootSaga} from './sagas';
import Search from './components/search/Search';
import PlaylistPage from './components/playlist/PlaylistPage';
import PlaylistCreate from './components/playlist/PlaylistCreate';
// import { Switch } from 'material-ui';



sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
     <BrowserRouter>
      <div>
        <MyApp/>
        <div>
          <Route exact path='/' component={PlaylistPage}  />
          <Route path='/login' component={Login}  />
          <Route path='/search' component={Search}  />
          <Route path='/sign-up' component={SignUp}  />
          <Route path='/profile' component={UserProfile}  />
          <Route path='/new-playlist' component={PlaylistCreate}  />
          
        </div>
      </div>
    </BrowserRouter>
  </Provider>,document.getElementById('root'));
registerServiceWorker();



{/* <Router history={history}>
      <Route path="/" component={App}>
        <Router path="sign-up" component={SignUp}/>
      </Route>
    </Router> */}
    // <BrowserRouter>
    //   <CookiesProvider>
    //     <ErrorBoundary>
    //       <App />
    //     </ErrorBoundary>
    //   </CookiesProvider>
    // </BrowserRouter>