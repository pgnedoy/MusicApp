import { SET_READY, 
         SET_SUGGESTIONS, 
         SET_SUGGESTIONS_DB } from '../actions/types'

const initialState = {
  // gapiReady: false,
  suggestions: "",
  suggestionsFromDB: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SUGGESTIONS:
      return {
        ...state,
        suggestions: action.payload
      };
    case SET_SUGGESTIONS_DB:
      return {
        ...state,
        suggestionsFromDB: action.payload    
      };
    default:
      return state;
  }
  return state
}
