import { combineReducers } from 'redux';
// import UserReducer from './UserReducer';
import ApiUserReducer from './ApiUserAuthorization';
import { routerReducer } from 'react-router-redux'
import FlagsReducer from './FlagsReducer';
import ProxyUserReducer from './ProxyUserReducer';
import Suggestions from './Suggestions';
import Playlist from './Playlist';
import Titles from './Titles';
import song from './song';

// const middleware = routerMiddleware(history)


export const allReducers = combineReducers({
  routing: routerReducer, 
  user: ApiUserReducer,
  proxyUser: ProxyUserReducer,
  flags: FlagsReducer,
  suggestions: Suggestions,
  playlist: Playlist,
  titles: Titles,
  currentSong: song
});