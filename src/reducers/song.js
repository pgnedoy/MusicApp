import { SET_SONG_DATA } from "../actions/types";

const initialState = {
  index: null,
  image: null,
  title: null,
  path: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SONG_DATA:
      return {  
        index: typeof action.payload.index != 'undefined' ? action.payload.index : null,
        image: action.payload.image,        
        title: action.payload.title,
        path: action.payload.path || null
      };  
    default:
      return state;
  }
  return state;
}
