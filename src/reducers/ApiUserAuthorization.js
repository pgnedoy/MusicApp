import {  API_LOGIN_REQUEST,
          API_LOGIN_SUCCESS,
          API_LOGIN_FAILURE,
          API_SIGNUP_REQUEST,
          API_SIGNUP_SUCCESS,
          API_SIGNUP_FAILURE, 
          UNSET_USER_DATA} from '../actions/types'
import { read_cookie } from 'sfcookies'
          

const initialState = {
  fetching: false,
  user:  {
    userId: Array.isArray(read_cookie("userId"))  ? null : read_cookie("userId"),
    userName: Array.isArray(read_cookie("userName")) ? null : read_cookie("userName")
  },
  // read_cookie("userId") !== [] ? read_cookie("userId") : null,
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case API_LOGIN_REQUEST:
      return { ...state, fetching: true, error: null };
      break;
    case API_LOGIN_SUCCESS:
      return { ...state, fetching: false, user: {
        userId: action.user._id,
        userName: action.user.username
      }};
      break;
    case API_LOGIN_FAILURE:
      return { ...state, fetching: false, user : {
        userId: null,
        userName: null
      } , error: action.error };
      break;
    case UNSET_USER_DATA:
      return { ...state, fetching: false, user : {
        userId: null,
        userName: null
      }, error: null };
      break; 
    case API_SIGNUP_REQUEST:
      return { ...state, fetching: true, error: null };
      break;
    case API_SIGNUP_SUCCESS:
      return { ...state, fetching: false, user: {
        userId: action.user._id,
        userName: action.user.username
      }};
      break;
    case API_SIGNUP_FAILURE:
      return { ...state, fetching: false, user : {
        userId: null,
        userName: null
      } , error: action.error };
      break;
    default:
      return state;
  }
}