import { SET_FALSE , SET_TRUE, SET_AUTHOR_TAG} from '../actions/types'

const initialState = {
  leftMenu: false,
  authorization: "",
  newPly: false,
  deletePlaylist: false,
  deleteSong: false,
  playlistPage: null,
  unmountingFlag: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_FALSE:
      return {
        ...state,
        [action.payload.flag]: action.payload.value
      };
    case SET_TRUE:
      return {
        ...state,
        [action.payload.flag]: action.payload.value     
      };
    case SET_AUTHOR_TAG:
      // console.log("action.payload", action.payload);
      return {
        ...state,
        authorization: action.payload   
      };
    // }

      
    default:
      return state;
  }
  return state
}
 