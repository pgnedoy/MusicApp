import { SET_USER_IMAGE , SET_USER_NAME, SET_USER_EMAIL, SET_USER_PASSWORD, SET_USER_CONFPASSWORD, UNSET_PROXY_DATA} from '../actions/types'

const initialState = {
  id: "",
  name: "",
  email: "",
  password: "",
  confpassword: "",
  image: ""
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_USER_NAME:
      return {
        ...state,
        name: action.payload
      };
    case SET_USER_IMAGE:
      return {
        ...state,
        image: action.payload
      };
    case SET_USER_EMAIL:
      return {
        ...state,
        email: action.payload
      };
    case SET_USER_PASSWORD:
      return {
        ...state,
        password: action.payload
      };
    case SET_USER_CONFPASSWORD:
      return {
        ...state,
        confpassword: action.payload
      };
    case UNSET_PROXY_DATA:
      return initialState;
    default:
      return state;
  }
  return state
}
