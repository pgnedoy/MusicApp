import { 
  // SET_PLAYLIST,
  // UNSET_PLAYLIST,
  // DELETE_SONG,
  API_DELETE_PLAYLIST_REQUEST,
  API_DELETE_PLAYLIST_SUCCESS,
  API_DELETE_PLAYLIST_FAILURE,
  API_GET_PLAYLIST_REQUEST,
  API_GET_PLAYLIST_SUCCESS,
  API_GET_PLAYLIST_FAILURE,
  API_DELETE_SONG_REQUEST,
  API_DELETE_SONG_SUCCESS,
  API_DELETE_SONG_FAILURE,
  API_UPDATE_PLAYLIST_REQUEST,
  API_UPDATE_PLAYLIST_SUCCESS,
  API_UPDATE_PLAYLIST_FAILURE, 
} from '../actions/types'

const initialState = {
  fetching: false,
  error: null, 
  playlistFromServer: null,
  playlist:[]
};

export default function(state = initialState, action) {
  switch (action.type) {
    case API_GET_PLAYLIST_REQUEST:
      return {
        ...state,
        fetching: true,
        error: null  
      };
    case API_GET_PLAYLIST_SUCCESS:
      return {
        ...state,
        fetching: false,
        playlist: action.playlists.plyForClient,
        playlistFromServer: action.playlists.playlist  
      };
    case API_GET_PLAYLIST_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error 
      };  
    //////////////////////////////////////////  
    case API_UPDATE_PLAYLIST_REQUEST:
      return {
        ...state,
        fetching: true,
        error: null 
      };
    case API_UPDATE_PLAYLIST_SUCCESS:
      return {
        ...state,
        fetching: false,        
        playlist: action.payload
      };
    case API_UPDATE_PLAYLIST_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error
      };
    ///////////////////////////////////////  
    case API_DELETE_SONG_REQUEST:
      return {
        ...state,
        fetching: true,
        error: null  
      };
    case API_DELETE_SONG_SUCCESS:
      return {
        ...state,
        fetching: false,
        playlist: action.props.payload.newPly 
      };
    case API_DELETE_SONG_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error 
      };  
    default:
      return state;
  }
  return state;
}
