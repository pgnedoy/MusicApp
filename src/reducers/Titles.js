import { ADD_TITLE, 
  GET_TITLES, 
  CURRENT_TITLE, 
  UNSET_TITLE, 
  SELECT_TITLE, 
  API_GET_TITLES_REQUEST,
  API_GET_TITLES_SUCCESS,
  API_GET_TITLES_FAILURE,
  API_DELETE_PLAYLIST_REQUEST,
  API_DELETE_PLAYLIST_SUCCESS,
  API_DELETE_PLAYLIST_FAILURE} from "../actions/types";



const initialState = {
  fetching: false,
  error: null,  
  titles: [],
  currentTitle: 0,
  selectTitle: 0
};

export default function(state = initialState, action) {
  // console.log("!!!!action", action);

  switch (action.type) {
    case ADD_TITLE:
      return {
        ...state,
        titles: state.titles.push(action.payload)
      };
    case API_GET_TITLES_REQUEST:
      return { ...state, fetching: true, error: null };
    case API_GET_TITLES_SUCCESS:
      return {
        ...state,
        titles: action.titles
      };
    case API_GET_TITLES_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error
      }; 
    case CURRENT_TITLE:
      return {
        ...state,
        currentTitle:action.payload
        // selectTitle: action.payload        
      };
    case UNSET_TITLE:
      return {
        ...state,
        selectTitle: state.currentTitle,
        currentTitle: 0,
      };
    case SELECT_TITLE:
      return {
        ...state,
        selectTitle: action.payload,
      };
    case API_DELETE_PLAYLIST_REQUEST:
      return {
        ...state,
        fetching: true,
        error: null  
      };
    case API_DELETE_PLAYLIST_SUCCESS:
      return {
        ...state, 
        titles: action.titles.reverse()
      };
    case API_DELETE_PLAYLIST_FAILURE:
      return {
        ...state,
        fetching: false,
        error: action.error 
      };  
      
    default:
      return state;
  }
  return state
}