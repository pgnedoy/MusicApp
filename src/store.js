import { createStore, compose,applyMiddleware } from "redux";
import { allReducers } from "./reducers/index";

import createSagaMiddleware from "redux-saga";
export const sagaMiddleware = createSagaMiddleware();

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
/* eslint-disable no-underscore-dangle */
export const store = createStore(allReducers, compose(applyMiddleware(sagaMiddleware), reduxDevTools))  ;
/* eslint-enable */ 