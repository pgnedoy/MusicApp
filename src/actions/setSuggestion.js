import { SET_SUGGESTIONS, SET_SUGGESTIONS_DB } from "./types";


export function setSuggestions(suggestions) {
  return {
    type: SET_SUGGESTIONS,
    payload: suggestions
  } 
}

export function setSuggestionsFromDB(suggestions) {
  return {
    type: SET_SUGGESTIONS_DB ,
    payload: suggestions
  } 
}