import { 
  SET_PLAYLIST, 
  UNSET_PLAYLIST, 
  DELETE_SONG, 
  API_DELETE_PLAYLIST_REQUEST,
  GET_PLAYLIST,
  API_GET_PLAYLIST_REQUEST,
  API_DELETE_SONG_REQUEST,
  // RESET_PLAYLIST,
  SET_SONG_DATA,
  API_UPDATE_PLAYLIST_REQUEST
 } from "./types";

export const setSongData = data => ({
    type: SET_SONG_DATA,
    payload: {
      index: data.index,
      image: data.image,
      title: data.title,
      path: data.path
    }
})

export const deleteSong = props => {
  let newPly = props.playlist.map(ply => ply);
  newPly.splice(props.index,1)
  return {
    type: API_DELETE_SONG_REQUEST,
    payload: {
      ...props,
      newPly
    }
  }
}

export const setPlaylist = (playlist, pReady) => {
  if (playlist.length && typeof pReady != 'undefind' ) {
    const ply = playlist.map((song, i) => ({
      num: i,
      path: song.path,
      id: song._id,
      title: song.title,
      image: song.image,
      time: song.time
    }));
    return ply;
  }else if (pReady) {
    return playlist
  }else{
    return [];
  }
}

export const updatePlaylist = data => ({
    type: API_UPDATE_PLAYLIST_REQUEST,
    payload: {...data}
})

export const getPlaylist = (userId, numPly) => ({
  type: API_GET_PLAYLIST_REQUEST,
  payload: {
    userId,
    numPly
  }
})

export const deletePlaylist = (userId, numPly) => ({
  type: API_DELETE_PLAYLIST_REQUEST,
  payload: {
    userId,
    numPly
  }
})

export const unSetPlaylist = () => ({
    type: UNSET_PLAYLIST,
    payload: []
})