import { SET_TRUE, SET_FALSE, SET_AUTHOR_TAG, DELETE_TITLE} from './types';

export const setTrue = (flag) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_TRUE,
    payload: {
      flag: flag,
      value: true
    }
  }
};

export const setFalse = (flag) => {
  // console.log("SetFalse worked");
  return {
    type: SET_FALSE,
    payload: {
      flag: flag,
      value: false
    }
  }
};

export const setAuthorizationFlag = (tag) => {
  // console.log("SetAuthorization worked", tag);
  return {
    type: SET_AUTHOR_TAG,
    payload: tag
  }
};

export const unSetAuthorizationFlag = () => {
  // console.log("SetAuthorization worked", tag);
  return {
    type: SET_AUTHOR_TAG,
    payload: ''
  }
};

// export const setDeleteFlag = (tag) => {
//   // console.log("SetAuthorization worked", tag);
//   return {
//     type: DELETE_TITLE,
//     payload: tag
//   }
// };

// export const unSetDeleteFlag = () => {
//   // console.log("SetAuthorization worked", tag);
//   return {
//     type: DELETE_TITLE,
//     payload: ''
//   }
// };



