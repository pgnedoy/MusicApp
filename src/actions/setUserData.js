import { API_LOGIN_REQUEST, API_SIGNUP_REQUEST, UNSET_USER_DATA } from "./types"
import { delete_cookie } from 'sfcookies'

export const setUserDataLogin = (e, pas) => {
  // console.log("SetTrue worked", flag);
  return {
    type: API_LOGIN_REQUEST,
    payload: {
      email: e,
      password: pas
    }
  }
};

export const setUserDataSignUp = (name, e, pas, confpas) => {
  // console.log("sign up ");
  return {
    type: API_SIGNUP_REQUEST,
    payload: {
      name: name,
      confpassword: confpas,
      email: e,
      password: pas
    }
  }
};

export const unSetUserData = () => {
  delete_cookie("userId");
  delete_cookie("userName");
  // console.log("unSet");
  return {
    type: UNSET_USER_DATA,
    payload: null
  }
};