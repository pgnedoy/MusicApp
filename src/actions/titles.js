import { ADD_TITLE, SET_TITLES, CURRENT_TITLE, UNSET_TITLE, SELECT_TITLE, API_GET_TITLES_REQUEST } from "./types";

export function addTitle(title) {
  return {
    type: ADD_TITLE,
    payload: title
  }
}

export function getTitles(userId) {
  console.log("getTitles", userId)
  return {
    type: API_GET_TITLES_REQUEST,
    payload: {
      userId,
      // currentTitle,

    }
  }
}

export function setTitles(titles) {
  // console.log("getTitles", titles)
  return {
    type: SET_TITLES,
    payload: titles
  }
}

export function currentTitle(num) {
  return {
    type: CURRENT_TITLE,
    payload: num
  }
}

export function setSelectTitle(num) {
  console.log("setSelectTitles",num)
  
  return {
    type: SELECT_TITLE,
    payload: num
  }
}

export function unSetCurrentTitle() {
  return {
    type: UNSET_TITLE,
    payload: null
  }
}