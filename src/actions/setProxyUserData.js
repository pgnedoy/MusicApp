import {SET_USER_EMAIL, 
        SET_USER_ID, 
        SET_USER_NAME, 
        SET_USER_PASSWORD, 
        SET_USER_IMAGE, 
        SET_USER_CONFPASSWORD,
        UNSET_PROXY_DATA } from './types';

export const setUserName = (name) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_NAME,
    payload: name
  }
};

export const setUserEmail = (email) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_EMAIL,
    payload: email
  }
};

export const setUserId = (id) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_ID,
    payload: id
  }
};

export const setUserPassword = (password) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_PASSWORD,
    payload: password
  }
};

export const setUserConfPassword = (password) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_CONFPASSWORD,
    payload: password
  }
};

export const setUserImage = (image) => {
  // console.log("SetTrue worked", flag);
  return {
    type: SET_USER_IMAGE,
    payload: image
  }
};

export const unSetProxyUserData = () => {
  // console.log("SetTrue worked", flag);
  return {
    type: UNSET_PROXY_DATA,
    payload: ""
  }
};

// export const setUserData
