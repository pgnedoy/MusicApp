import React, { Component } from "react";
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import { Switch, Route, Link } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { read_cookie } from 'sfcookies'
import { deletePlaylist } from '../../actions/setPlaylist'
import Grid from '@material-ui/core/Grid';
import LibraryMusic from '@material-ui/icons/LibraryMusic';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  playlist:{
    margin: theme.spacing.unit,
    padding: "8px 213px"
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class Buttons extends Component {
  constructor(props){
    super(props)
  }

  deletePlaylist = () => {
    const userId = read_cookie("userId");
    this.props.deletePlaylist(userId, this.props.titles.currentTitle);
  }

  render() {
    const { classes } = this.props;
    if (this.props.playlistPageFlag) {
      return (
        <div>
          <Grid container spacing={24}>
            <Grid item xs></Grid>
            <Grid item xs={4}>
              <Link to="/search"  style={{ textDecoration: 'none' }}>
                <Button className={classes.button} variant="raised" color="secondary">
                  Add song
                  <Add className={classes.rightIcon} />
                </Button>
              </Link>
            </Grid>
            <Grid item xs={6}>
              <Button className={classes.button} onClick = {this.deletePlaylist} variant="raised" color="secondary">
                Delete playlist
                <Delete className={classes.rightIcon} />
              </Button>
            </Grid>
          </Grid>
        </div>
      );
    }else {
      return (
        <div>
          <Grid container spacing={24}>
            <Grid item xs={4}>
              <Link to="/"  style={{ textDecoration: 'none' }}>
                <Button className={classes.playlist} variant="raised" color="secondary">
                  playlist
                <LibraryMusic className={classes.rightIcon} />
                </Button>
              </Link>
            </Grid>
          </Grid>
        </div>
      )
    }
  }
}

const mapStateToProps = state => {
  return { 
    playlistPageFlag: state.flags.playlistPage,
    titles: state.titles
  }
};

const matchDispatchToProps = dispatch => {
  return bindActionCreators({deletePlaylist}, dispatch);
}

Buttons.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(Buttons));
