import React, { Component } from "react";
import update from "immutability-helper";
import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import Card from "./Card";
import config from "../../config.json";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { read_cookie } from 'sfcookies'
import { 
  setPlaylist, 
  updatePlaylist, 
  unSetPlaylist 
} from '../../actions/setPlaylist'
import { setFalse } from '../../actions/flagsСhange'
import { CLIENT_RENEG_LIMIT } from "tls";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Container extends Component {
  constructor(props) {
    super(props);
    this.moveCard = this.moveCard.bind(this);
    
  }

  // shouldComponentUpdate(nextProps){
  //   console.log("nextProps", nextProps.playlist);
  //   console.log("this.props", this.props.playlist);
  //   return true
  //   // if (this.props.playlist.length !== nextProps.playlist.length) {
  //   //   console.log("true")
  //   //   return true;
  //   // }
  //   // return false;
  // }


  componentDidUpdate(prevProps){
    // console.log("prevProps", prevProps.playlist);
    // console.log("this.props", this.props.playlist);
  }

  componentWillUnmount() {
    this.props.unSetPlaylist();
  }
  
  
  moveCard(dragIndex, hoverIndex) {
    const songs = this.props.playlist;
    const dragCard = songs[dragIndex];
    let newPly  = update(this.props.playlist, {
      $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]]
    })
    // this.requestChange(newPly);
    this.props.updatePlaylist(newPly)
  }

  createCard = () => {
    const songs = this.props.playlist
    const {classes} = this.props;
    let list = [];
    for (let i = 0; i < songs.length; i++) {
      list[i] = (
        <div className={classes.root}>
          <Grid container spacing={24}>
            <Grid item xs></Grid>
            <Grid item xs={10}>
              <Card
                id={songs[i].id}
                path={songs[i].path}
                key={songs[i].num}
                index={i}
                image={songs[i].image}
                num={songs[i].num}
                title={songs[i].title}
                time={songs[i].time}              
                moveCard={this.moveCard}
              />
            </Grid>
          </Grid>
        </div>
      );
    }
    return list;
  }

  render() {
    console.log("container render")
    return <div>{this.createCard()}</div>;
  }
}

const mapStateToProps = state =>{
  return { 
    titles: state.titles,
    playlist: state.playlist.playlist,
    deleteSong: state.flags.deleteSong
  }
};

const matchDispatchToProps = dispatch => {
  return bindActionCreators({ unSetPlaylist, updatePlaylist }, dispatch);
}

Container.propTypes = {
  classes: PropTypes.object.isRequired,
};


Container = DragDropContext(HTML5Backend)(Container);
export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(Container));
