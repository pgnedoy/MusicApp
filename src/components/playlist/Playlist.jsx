import React, { Component } from "react";
import Container from "./Container";
import PropTypes from "prop-types";
import Button from "material-ui/Button";
import { platform } from "os";
import config from "../../config.json";
import { MenuItem } from "material-ui/Menu";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { read_cookie } from 'sfcookies'
import { currentTitle } from '../../actions/titles'
// import { setFalse } from '../../actions/flagsСhange'
import { setPlaylist, getPlaylist  } from '../../actions/setPlaylist'

class Playlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playlist: "",
      suggestions: ""
    };
    this.getPlaylist = this.getPlaylist.bind(this);
  }
  
  componentWillMount() {
    this.getPlaylist();
  }
  

  // shouldComponentUpdate(nextProps){
  //   if (this.props.ply !== nextProps.ply) {
  //   console.log("!!!", this.props.ply === nextProps.ply) 
      
  //     console.log("true")
  //     return true;
  //   }
  //   return false;
  // }

  getPlaylist = () => {
    const userId = read_cookie("userId");
    const numPly = this.props.titles.currentTitle;
    this.props.getPlaylist(userId, numPly);
  }
  
  render() {
    console.log("Playlist re-render", this.props.ply);
    return (
      <div>{ this.props.ply.length ? <Container /> : null }</div>
    );
  }
}

const mapStateToProps = state => ({
    titles: state.titles,
    ply: state.playlist.playlist
});

const matchDispatchToProps = dispatch => {
  return bindActionCreators({setPlaylist, currentTitle, getPlaylist}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Playlist);


