import React, { Component } from 'react';
import PlaylistPanel from './PlaylistPanel'
import CSSGrid from './testGrid'
import logo from "../../phone.jpg"
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setTrue } from '../../actions/flagsСhange'
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { setFalse } from '../../actions/flagsСhange'
import Playlist from './Playlist'






// import Playlist from './Playlist'
const styles = theme => ({
  mainDiv:{
    backgroundImage: `url(${logo})`,
    height: "1080px",
    backgroundRepeat: "no-repeat"
  }
  
})

class PlaylistPage extends Component {
  componentDidMount(){
    this.props.setTrue("playlistPage");
  }
  componentWillUnmount(){
    this.props.setFalse("playlistPage");
  }

  render() {
    const { classes } = this.props;    
    return (
      <div className={classes.mainDiv}>
        <PlaylistPanel />
        <Playlist />
      </div>
    );
  }
}

function mapStateToProps(state){
  return { 
    titles: state.titles,
    newPly: state.flags.newPly
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({setTrue, setFalse }, dispatch);
}

PlaylistPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(PlaylistPage));
