import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import SkipPreviousIcon from '@material-ui/icons/SkipPrevious';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import Player from "../Player"
import  Buttons  from './Buttons'
import logo from "../../image.jpg"
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { zIndex } from 'material-ui/styles';



const styles = theme => ({
  mainDiv:{
    position: 'fixed',
    padding: "110px",
    zIndex: 1
  },
  card: {
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
    height: 151,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  playIcon: {
    height: 38,
    width: 38,
  },
});

function MediaControlCard(props) {
  const { classes, theme, songData } = props;

  return (
    <div className={classes.mainDiv}>
      <Card className={classes.card}>
        <div className={classes.details}>
          <CardContent className={classes.content}>
          <marquee behavior="scroll" direction="left">{songData.title}</marquee>
          
            {/* <Typography variant="headline"></Typography> */}
            {/* <Typography variant="subheading" color="textSecondary">
              Mac Miller
            </Typography> */}

            <Player />
            <IconButton aria-label="Previous">
              {theme.direction === 'rtl' ? <SkipNextIcon /> : <SkipPreviousIcon />}
            </IconButton>
            <IconButton aria-label="Next">
              {theme.direction === 'rtl' ? <SkipPreviousIcon /> : <SkipNextIcon />}
            </IconButton>
          </CardContent>
        
          {/* className={classes.controls}> */}
            {/* <Player /> */}
        
        </div>
        <CardMedia
          className={classes.cover}
          image={songData.image}
          title="Live from space album cover"
        />
        
      </Card>
      <Buttons />
      
    </div>
  );
}
function mapStateToProps(state){
  return { 
    songData: state.currentSong,
  }
};
MediaControlCard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

// export default withStyles(styles)(MediaControlCard);
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(MediaControlCard));
