import React from "react";
import Button from "material-ui/Button";
import TextField from "material-ui/TextField";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";
import { Switch, Route, Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import config from "../../config.json";
import { read_cookie } from 'sfcookies'
import { setFalse } from '../../actions/flagsСhange'
import { addTitle } from '../../actions/titles'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'

class PlaylistCreate extends React.Component {
  async createPlaylist() {
    const resp = await fetch(
      `http://${config.server}:${config.port}/playlist/create`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({
          userId: read_cookie("userId"),
          title: this.state.name
        })
      }
    );
    try {
      this.props.setFalse("newPly");
      this.props.addTitle(this.state.name);
    } catch (error) {
      // this.props.setError("Error");
      return;
    }
  }

  handleOkey = () => {
    if (this.state.name) {
      this.createPlaylist();
    } else {
      ;
    }

  };
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  render() {
    if (!this.props.newPly) {
      return <Redirect to="/" />
    }
    return (
      <div>
        <Dialog
          open={true}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <DialogContentText>Input new playlist's title!.</DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="PLAYLIST"
              onChange={this.handleChange("name")}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleOkey} color="primary">
              OKEY
            </Button>
            <Button onClick={() => this.props.setFalse("newPly")} color="primary">
              CENCEl
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}


function mapStateToProps(state){
  return { 
    titles: state.titles.titles,
    newPly: state.flags.newPly 
  }
};
function matchDispatchToProps(dispatch){
  return bindActionCreators({setFalse, addTitle}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(PlaylistCreate);
