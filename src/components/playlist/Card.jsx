import React, { Component } from "react";
import PropTypes from "prop-types";
import { findDOMNode } from "react-dom";
import { DragSource, DropTarget } from "react-dnd";
import ItemTypes from "./ItemTypes";
import { withStyles } from "material-ui/styles";
import Grid from '@material-ui/core/Grid';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setSongData } from '../../actions/setPlaylist'
import { deleteSong } from '../../actions/setPlaylist'
import config from "../../config.json";
import { read_cookie } from 'sfcookies'
import { setTrue } from '../../actions/flagsСhange'

const style = {
  width: "375px",
  border: "3px solid #999",
  padding: "1rem 1rem",
  marginBottom: ".5rem",
  backgroundColor: "#eee",
  container: {
    display: "flex",
  }
};

const cardSource = {
  beginDrag(props) {
    return {
      num: props.num,
      index: props.index
    };
  }
};

const cardTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;
    if (dragIndex === hoverIndex) {
      return;
    }
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
    const clientOffset = monitor.getClientOffset();
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }
    props.moveCard(dragIndex, hoverIndex);
    monitor.getItem().index = hoverIndex;
  }
};

class Card extends React.Component {
  constructor(props) {
    super(props);
  }
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    title: PropTypes.string.isRequired,
    moveCard: PropTypes.func.isRequired
  };
  

  handleClickToDelete = index => {
    const { playlist, currentSong } = this.props;
    const data = {
      numPly: this.props.titles.currentTitle,
      userId: read_cookie("userId"),
      playlist: playlist,
      index
    }
    let songData = {}   
    if ( index === currentSong.index) {
      if (index === playlist.length - 1 ) {
        songData = this.setNextSongData(0, playlist);
      } else {
        songData = this.setNextSongData(index + 1, playlist);  
      }
      this.props.setSongData(songData);
    } else if (playlist.length === 1) {
      this.props.setSongData(songData);
    }
    this.props.deleteSong(data);
  }

  setNextSongData = (index, playlist) => ({
    index: index,
    title: playlist[index].title,
    image: playlist[index].image,
    path: playlist[index].path
  }) 

  render() {
    const { num, image, index, path, title, time, isDragging, connectDragSource, connectDropTarget } = this.props;
    const opacity = isDragging ? 0 : 1;
    console.log("card", this.props)
    return connectDragSource(
      connectDropTarget(
        <div style={style.container}>
          <div
            style={{ ...style, opacity }}
            onClick={() => this.props.setSongData({index, image, title, path})}
          >
            <Grid container spacing={12}>
              <Grid item xs={10}>
                {title}
              </Grid>
              <Grid item xs style={{textAlign: "right"}}>
                {time}  
              </Grid>            
            </Grid> 
          </div>
          <div onClick={() => this.handleClickToDelete(index)}>
            <i style={{ lineHeight: 2.5 }} class="material-icons">
              clear
            </i>
          </div>
        </div>
      )
    );
  }
}

Card = DropTarget(ItemTypes.CARD, cardTarget, connect => ({
  connectDropTarget: connect.dropTarget()
}))(Card);
Card = DragSource(ItemTypes.CARD, cardSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))(Card);

const mapStateToProps = state => {
  return { 
    titles: state.titles,
    playlist: state.playlist.playlist,
    currentSong: state.currentSong
  }
};

const matchDispatchToProps = dispatch => {
  return bindActionCreators({ setSongData, deleteSong, setTrue }, dispatch);
}

export default withStyles(style)(connect(mapStateToProps,matchDispatchToProps)(Card));
