import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Tabs, { Tab } from "material-ui/Tabs";
import config from "../../config.json";
import { Switch, Route, Link } from "react-router-dom";
import PlaylistCreate from "./PlaylistCreate";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { read_cookie } from 'sfcookies'
import { getTitles, currentTitle, setTitles } from '../../actions/titles'
import { setTrue } from '../../actions/flagsСhange'
import { Redirect } from "react-router-dom";
// import Playlist from './Playlist'
import { unSetPlaylist } from '../../actions/setPlaylist'
import { unSetCurrentTitle } from '../../actions/titles'
import Typography from "material-ui/Typography";
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import ImageSong from "./ImageSong";


function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = theme => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: `${theme.spacing.unit * 3}px`,
    // backgroundImage : `${logo}`
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing.unit,
  },
  divider: {
    margin: `${theme.spacing.unit * 2}px 0`,
  },
 
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
});

class PlaylistPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      playlistCreate: false,
      open: true,
      value: 0,
      playlistsTitles: undefined,
      playlists: [],
      qty: 0
    };
  }

  componentDidMount() {
    this.props.getTitles(read_cookie("userId"));
  }

  componentWillUnmount(){
    this.props.unSetCurrentTitle();
  }

  

  openPlaylistCreate = () => {
    this.setState({
      playlistCreate: true
    });
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    if (this.props.newPly) {
      return <Redirect to="/new-playlist" />
    }
    if (this.props.titles.titles.length) {
      return (
        <div>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={this.handleChange}
              scrollable
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
            >
              {this.props.titles.titles.map((title, i) => <Tab label={title} onClick = {()=>this.props.currentTitle(i)}/>)}
            <i class="material-icons" onClick={() => this.props.setTrue("newPly")}>
                add
              </i> 
            </Tabs>
          </AppBar>
          <Grid container spacing={24}>
            <Grid item xs={6}>
              {/* <Playlist /> */}
            </Grid>
          </Grid> 
        </div>
      );
    }else {
      return (
        <div>
          <h2>Create yourself PLAYLIST!!!</h2>
          <h4>press button</h4>
          <i class="material-icons" onClick={() => this.props.setTrue("newPly")}>
            add
          </i>
        </div>
      );
    }
  }
}

function mapStateToProps(state){
  return { 
    titles: state.titles,
    newPly: state.flags.newPly
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({getTitles, setTrue, currentTitle, unSetPlaylist, unSetCurrentTitle, setTitles }, dispatch);
}
 

PlaylistPanel.propTypes = {
  classes: PropTypes.object.isRequired
};


// matchDispatchToProps
export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(PlaylistPanel));
