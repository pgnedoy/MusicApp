import React, { Component } from "react";
import ReactAudioPlayer from "react-audio-player";
import { Grid } from "material-ui";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setSongData } from "../actions/setPlaylist"

import config from "../config.json";

class Player extends Component {

  setNextSong = () => {
    const { playlist, songData } = this.props;
    let index, nextSong;
    if ( playlist[songData.index].path == songData.path ) {
      index = songData.index + 1 < playlist.length ? songData.index + 1 : 0;
      nextSong = this.setNextSongData(index, playlist);     
    } else {
      let i = 0;
      while (playlist[i].path != songData.path) {
        i++;
      }
      index = i + 1 < playlist.length ? i + 1 : 0;
      nextSong = this.setNextSongData(index, playlist);
    }
    this.props.setSongData(nextSong); 
  }

  setNextSongData = (index, playlist) => ({
    index: index,
    title: playlist[index].title,
    image: playlist[index].image,
    path: playlist[index].path
  }) 


  render() {
    const { playlist, songData } = this.props;
    const songPath = songData.path || null;
    const src = `http://${config.server}:${config.port}/${songPath}`;
    return (
      <Grid item xs={12} style={{marginTop: "20px" }}>
        <ReactAudioPlayer
          src={src}
          autoPlay
          onEnded={() => this.setNextSong()}
          controls
        />
      </Grid>
    );
    
    // return null;
  }
}

const mapStateToProps = state => {
  return { 
    playlist: state.playlist.playlist,
    songData: state.currentSong
  }
};

const matchDispatchToProps = dispatch => {
  return bindActionCreators({
    setSongData
  }, dispatch);
}

export default (connect(mapStateToProps, matchDispatchToProps)(Player));
