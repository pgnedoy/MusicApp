import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import config from "../../config.json";
import { read_cookie } from 'sfcookies'
import { withStyles } from "material-ui/styles";
import { setSongData } from "../../actions/setPlaylist"

const style = {
  border: "1px dashed gray",
  padding: "0.5rem 1rem",
  marginBottom: ".5rem",
  backgroundColor: "white"
  // cursor: "move"
};

class SuggestionFromDB extends Component {
  constructor(props) {
    super(props);
  }
  
  async handleClickItemFromDB(props) {
    let userId = read_cookie("userId");
    let numPly = this.props.titles.titles.length - this.props.titles.currentTitle - 1;
    await fetch(`http://${config.server}:${config.port}/song/songFromDB`, {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        userId: userId,
        songId: props.  id,
        numPly: numPly
      })
    }).then(() => {
      console.log("its okey");
    });
  }

  render() {
    return (
      <div style={{ ...style }} onClick={()=>this.handleClickItemFromDB(this.props)}>
        {this.props.title}
      </div>
    );
  }
}

const mapStateToProps = state => ({ titles: state.titles });

const matchDispatchToProps = dispatch => {
  return bindActionCreators({ setSongData }, dispatch);
}

export default withStyles(style)(connect(mapStateToProps, matchDispatchToProps)(SuggestionFromDB));

