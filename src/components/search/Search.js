import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid } from "material-ui";
import Button from "material-ui/Button";
import Downshift from "downshift";
import TextField from "material-ui/TextField";
import Paper from "material-ui/Paper";
import { withStyles } from "material-ui/styles";
import config from "../../config.json";
import Suggestion from "./Suggestion";
import SuggestionFromDB from "./SuggestionFromDB";
// import SelectPlaylist from "./SelectPlaylist";
import { setSuggestionsFromDB, setSuggestions } from "../../actions/setSuggestion";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { read_cookie } from 'sfcookies'
// import { setSongPath } from '../../actions/setPlaylist'
import SelectPlaylist from "./SelectPlaylist";




const styles = theme => ({
  textField: {
    width: "100%"
  }
});

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gapiReady: false,
      suggestions: "",
      suggestionsFromDB: ""
    };
    this.getItemsFromYoutube = this.getItemsFromYoutube.bind(this);
    this.getItemsFromDB = this.getItemsFromDB.bind(this);
  }
  
  async getItemsFromDB() {
    let reg = /.+/g;
    let songsFromDB;
    let result = this.state.inputValue.match(reg);
    const resp = await fetch(
      `http://${config.server}:${config.port}/song/findSongs`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({
          title: result[0]
        })
      }
    );
    try {
      songsFromDB = await resp.json();
      this.props.setSuggestionsFromDB(songsFromDB);
    } catch (error) {
      this.props.setError("Error");
      return;
    }
  }

  getItemsFromYoutube() {
    const request =
      gapi &&
      gapi.client.youtube.search.list({
        part: "snippet",
        q: `${this.state.inputValue} music`,
        maxResults: 15,
        type: "video"
      });
    request.execute(resp => {
      const suggestions = resp.items.map(item => ({
        label: item.snippet.title,
        videoId: item.id.videoId,
        image: item.snippet.thumbnails.high.url
      }));
      let sugs;
      Promise.all(suggestions.map(item => this.getDurationFromYoutube(item.videoId)))
      .then(resp => {
        sugs = suggestions.map((item, i) => {
          let duration = resp[i].items[0].contentDetails.duration;
          let time = this.getTime(duration);
          return {
            ...item,
            time
          }
        })
        this.props.setSuggestions(sugs);//// 
      })
    });
  }
  

  getTime(duration) {
    const hr = new RegExp("(\\d+)H");
    const mr = new RegExp("(\\d+)M");
    const sr = new RegExp("(\\d+)S");

    const h = hr.exec(duration);
    const m = mr.exec(duration);
    const s = sr.exec(duration);
    
    const newstr = h ? 
      `${h["1"]}:${m ? m["1"] : "00"}:${s ? s["1"]-1 : "00"}` :
      `${m ? m["1"] : "00"}:${s ? s["1"]-1 : "00"}`;
    return newstr;
  }

  getDurationFromYoutube(id){
    const req = 
    gapi &&
    gapi.client.youtube.videos.list({
      part: "contentDetails",
      id,
    });
    return new Promise((resolve) => req.execute(resp => {
      console.log("!!!Resp", resp);
      resolve(resp)
    }))    
  }

  handleClickFinding(fn1, fn2) {
    fn1();
    fn2();
  }

  createListOfSongsFromYouTube() {
    // console.log("!!!");
    var list;
    list = this.props.sug.suggestions.map(element => (
      <Suggestion
        suggestion={element}
        handleClick={this.handleClickItemFromYT}
      />
    ));
    return list;
  }
  createListOfSongsFromDB() {
    console.log("From DB", this.props.sug.suggestionsFromDB);
    var list;

    list = this.props.sug.suggestionsFromDB.map(element => (
      <SuggestionFromDB
        id={element._id}
        path={element.path}
        title={element.title}
        image={element.image}
        handleClick={this.handleClickItemFromDB}
      />
    ));
    return list;
  }

  
  loadYoutubeApi() {
    const script = document.createElement("script");
    script.src = "https://apis.google.com/js/client.js";
    const API_KEY = "AIzaSyAyOSj48aaR3PujaFQIMfvh22fRK0HrYs0";
    /*global gapi*/
    script.onload = () => {
      gapi.load("client", () => {
        gapi.client.setApiKey(API_KEY);
        gapi.client.load("youtube", "v3", () => {
          this.setState({ gapiReady: true });
        });
      });
    };
    document.body.appendChild(script);
  }

  componentDidMount() {
    this.loadYoutubeApi();
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  render() {
    let listDB;
    let listYT;
    if (this.props.sug.suggestionsFromDB != "") {
      listDB = this.createListOfSongsFromDB();
      // console.log("list DB", listDB);
    }
    if (this.props.sug.suggestions != "") {
      listYT = this.createListOfSongsFromYouTube();
    }
    const { classes, theme } = this.props;
    return this.state.gapiReady ? (
      <div>
        <Grid container>
          <Grid item xs />
          <Grid item xs={6}>
            <TextField
              id="search"
              label="Search field"
              type="search"
              className={classes.textField}
              onChange={this.handleChange("inputValue")}
              margin="normal"
            />
          </Grid>
          <Grid item xs />
        </Grid>
        <div>
          {/* <SelectPlaylist /> */}
          <Button onClick={()=> this.handleClickFinding(this.getItemsFromDB, this.getItemsFromYoutube)} raised color="">
            send
          </Button>
          {/* <SelectPlaylist hashTitles={this.props.hashTitles} /> */}
        </div>

        {this.props.sug.suggestionsFromDB != "" ? <div>{listDB}</div> : null}
        {this.props.sug.suggestions != "" ? <div>{listYT}</div> : null}
      </div>
    ) : null;
  }
}

function mapStateToProps(state){
  return { 
    sug: state.suggestions
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    setSuggestionsFromDB,
    setSuggestions,
    // setSongPath
  }, dispatch);
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, matchDispatchToProps)(Search));
