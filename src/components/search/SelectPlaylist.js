import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setSelectTitle } from '../../actions/titles'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class SelectPlaylist extends React.Component {
  state = {
    age: '',
    name: 'hai',
  };

  handleChange = (event, i) => {
    this.props.setSelectTitle(i)
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { classes } = this.props;
    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
            <Select
              value={this.state.age}
              onChange={this.handleChange}
              displayEmpty
              name="age"
              className={classes.selectEmpty}
            >
              {this.props.titles.map((title, i) => <MenuItem value={i}>{title}</MenuItem>)}
            </Select>
          </FormControl>
      </form>
    );
  }
}

function mapStateToProps(state){
  return { 
    titles: state.titles.titles
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    setSelectTitle
  }, dispatch);
}


SelectPlaylist.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(SelectPlaylist));