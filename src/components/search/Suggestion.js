import React, { Component } from "react";
import { setSongData } from "../../actions/setPlaylist"
import { MenuItem } from "material-ui/Menu";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import config from "../../config.json";
import {read_cookie} from 'sfcookies'

class Suggestion extends Component {
  constructor(props) {
    super(props);
  }
  
  handleClick = suggestion => {
    const item = suggestion;
    const numPly = this.props.titles.selectTitle;
    fetch(`http://${config.server}:${config.port}/song`, {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        userId: read_cookie("userId"),
        youtubeId: item.videoId,
        title: item.label.replace(/\//g, ""),
        image: item.image,
        numPly,
        time:suggestion.time
      })
    })
      .then(resp => resp.json())
      .then(song => this.props.setSongData(song.path));
  }

  render() {
    const {
      suggestion,
      index,
      itemProps,
      theme,
      highlightedIndex,
      selectedItem
    } = this.props;
    const isHighlighted = highlightedIndex === index;
    const isSelected = selectedItem === suggestion.label;
    return (
      <MenuItem
        {...itemProps}
        key={this.props.suggestion.title || this.props.suggestion.label}
        selected={isHighlighted}
        component="div"
        onClick={() => this.handleClick(this.props.suggestion)}
      >
      {this.props.suggestion.title || this.props.suggestion.label}
      {this.props.suggestion.time}
      </MenuItem>
    );
  }
}

const mapStateToProps = state => ({titles: state.titles})

const matchDispatchToProps = dispatch => {
  return bindActionCreators({
    setSongData,
  }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Suggestion);
