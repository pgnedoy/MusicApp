import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import IconButton from "material-ui/IconButton";
import MenuIcon from "material-ui-icons/Menu";
import { Switch, Route, Link } from "react-router-dom";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'

import { setTrue } from '../actions/flagsСhange'
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies'
import ImageSong from "./playlist/ImageSong"
import Grid from '@material-ui/core/Grid';




// import { mapStateToProps } from 'r'
import Authorization from "./authorization/Authorization";
import UserMenu from './userMenu/UserMenu'
import Player from "./Player";

const styles = {
  root: {
    width: "100%"
  },
  flex: {
    display: "flex",
    flex: 2
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class MyAppBar extends React.Component {
  render() {
    const { classes } = this.props;
    // console.log("userId", Array.isArray( read_cookie("userId") )); 
    // console.log("user", this.props.user.userName);
    return (
      
      // <div className={classes.root}>
      <div >
      
        <AppBar position="static">
          <Toolbar>
            <IconButton
              onClick={() => this.props.setTrue("leftMenu")}
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
            >
              <Button component={Link} to="/" color="inherit">
                Flow
              </Button>
              {/* <Player /> */}
            </Typography>
            {this.props.user.userName ? <UserMenu /> : <Authorization />}
            {/* <Authorization /> */}
          </Toolbar>
        </AppBar>
        <Grid container spacing={24} >
          <Grid item xs={6}>
            {/* <Playlist /> */}
          </Grid>
          <Grid item xs={6}>
            <ImageSong />
          </Grid>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state){
  return { 
    leftMunu: state.flags.leftMunu,
    user: state.user.user
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({setTrue: setTrue}, dispatch);
}


MyAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(MyAppBar));
