import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import MenuItem from "material-ui/Menu/MenuItem";
import Button from "material-ui/Button";
import classNames from "classnames";
import TextField from "material-ui/TextField";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";

import Stepper, { Step, StepLabel, StepContent } from "material-ui/Stepper";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography";
import Icon from "material-ui/Icon";
import { Redirect } from "react-router";
import { styles } from "./Login.styles";
import Avatar from "material-ui/Avatar";
import { setUserEmail, setUserPassword, unSetProxyUserData } from '../../actions/setProxyUserData'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setAuthorizationFlag, unSetAuthorizationFlag } from '../../actions/flagsСhange'
import { setUserDataLogin } from '../../actions/setUserData'
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies'

// import MyAvatar from "./Avatar";

class Login extends React.Component {
  setData(data) {
    this.setState({ avatarData: data });
  }
  handleChange = name => event => {
    (name === 'pass') ? 
    this.props.setUserPassword(event.target.value) : 
    this.props.setUserEmail(event.target.value)
  };
  
  render() {
    const { classes } = this.props;
    if (this.props.user.userId) {

      bake_cookie("userId", this.props.user.userId);
      bake_cookie("userName", this.props.user.userName);
      this.props.unSetAuthorizationFlag();
      this.props.unSetProxyUserData();
      
      // console.log("id", read_cookie("userI"));
      // bake_cookies("u", this.props.user._id);
      // bake_cookies("userId", this.props.user._id);
      return <Redirect to="/" />;
    } else if (this.props.authorization === 'cancel') {
      this.props.unSetAuthorizationFlag();
      
      this.props.unSetProxyUserData();      
      return <Redirect to="/" />;
    }
    return (
      <div>
        <Dialog
          open={true}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Login</DialogTitle>
          <div className={classes.row}>
            <div className={classes.container}>
              <TextField
                id="email"
                label="Email"
                className={classes.textField}
                value={this.props.proxyUser.email}
                onChange={this.handleChange("email")}
                margin="normal"
              />

              <TextField
                id="password"
                label="Password"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
                onChange={this.handleChange("pass")}
              />
            </div>
            <div className={classes.container}>
            </div>
          </div>
          <DialogActions>
            <Button onClick={()=> this.props.setAuthorizationFlag("cancel")} color="primary">
              Cancel
            </Button>
            <Button
              onClick={()=>this.props.setUserDataLogin(this.props.proxyUser.email, this.props.proxyUser.password)}
              className={classes.button}
              raised
              color="primary"
            >
              send
            </Button>
            <Button color="primary" onClick={()=> this.props.setAuthorizationFlag("sign-up")}>
              Sign up
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state){
  return { 
    user: state.user.user,
    proxyUser: state.proxyUser,
    authorization:state.flags.authorization
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    setAuthorizationFlag,
    setUserEmail,
    setUserPassword,
    setUserDataLogin,
    unSetAuthorizationFlag,
    unSetProxyUserData,
  }, dispatch);
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};


export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(Login));
