import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import MenuItem from "material-ui/Menu/MenuItem";
import Button from "material-ui/Button";
import classNames from "classnames";
import TextField from "material-ui/TextField";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";

import Stepper, { Step, StepLabel, StepContent } from "material-ui/Stepper";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography";

import Icon from "material-ui/Icon";
import { Redirect } from "react-router-dom";
// import config from "../config.json";
import { styles } from "./SignUp.styles";
import Avatar from "material-ui/Avatar";

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setAuthorizationFlag, unSetAuthorizationFlag } from '../../actions/flagsСhange'
import { setUserEmail, setUserPassword, setUserName, setUserConfPassword, unSetProxyUserData } from '../../actions/setProxyUserData'
import { setUserDataSignUp } from '../../actions/setUserData'
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies'


// import MyAvatar from "./Avatar";

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      open: true,
      email: "",
      name: "",
      pass: "",
      passConf: "",
      avatarData: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.setData = this.setData.bind(this);
    // this.handleChange1 = this.handleChange1.bind(this);
  }
  // handleChange1 = event => {
  //   console.log("Selected file:", event.target.files[0]);
  // };
  setData(data) {
    this.setState({ avatarData: data });
  }

  handleChange = name => event => {
    switch (name) {
      case "pass":
        this.props.setUserPassword(event.target.value)
        break;
      case "email":
        this.props.setUserEmail(event.target.value)
        break;
      case "name":
        this.props.setUserName(event.target.value)
        break;
      case "passConf":
        this.props.setUserConfPassword(event.target.value)
        break;
      default:
        break;
    }
    // (name === 'pass') ? 
    // this.props.setUserPassword(event.target.value) : 
    // this.props.setUserEmail(event.target.value)
  };
  handleClose = () => {
    // this.setState({ open: false });
    this.setState({ redirect: true });
  };

  async handleClick() {
    // let user;
    // const resp = await fetch(`http://${config.server}:${config.port}/user`, {
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json"
    //   },
    //   method: "POST",
    //   body: JSON.stringify({
    //     email: this.state.email,
    //     username: this.state.name,
    //     password: this.state.pass,
    //     passwordConf: this.state.passConf,
    //     imagePath: this.state.avatarData ? this.state.avatarData.path : ""
    //   })
    // });
    // try {
    //   user = await resp.json();
    // } catch (error) {
    //   this.props.setError("Error");
    //   return;
    // }
    // console.log(user);
    // this.props.setUserName(user.username);
    // this.props.setEmail(user.email);
    // this.props.setUserId(user._id);
    // this.setState({ redirect: true });
    // this.props.setAvatarData(user.imagepath);
  }

  render() {
    const { classes } = this.props;
    if (this.props.user.userId) {
      bake_cookie("userId", this.props.user.userId);
      bake_cookie("userName", this.props.user.userName);
      this.props.unSetAuthorizationFlag();
      this.props.unSetProxyUserData();
      return <Redirect to="/" />;
    } else if (this.props.authorization === 'login') {
      this.props.unSetProxyUserData();      
      return <Redirect to="/login" />;
    } else if (this.props.authorization === 'cancel') {
      this.props.unSetAuthorizationFlag();
      this.props.unSetProxyUserData();      
      return <Redirect to="/" />;
    }
    return (
      // <h2>@!!</h2>
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Registration</DialogTitle>
          <div className={classes.row}>
            <div className={classes.container}>
              <TextField
                id="name"
                label="Name"
                className={classes.textField}
                value={this.props.proxyUser.name}
                onChange={this.handleChange("name")}
                margin="normal"
              />

              <TextField
                id="email"
                label="Email"
                className={classes.textField}
                value={this.props.proxyUser.email}
                onChange={this.handleChange("email")}
                margin="normal"
              />

              <TextField
                id="password"
                label="Password"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
                onChange={this.handleChange("pass")}
              />

              <TextField
                id="passwordConf"
                label="Password confim"
                className={classes.textField}
                type="password"
                autoComplete="current-password"
                margin="normal"
                onChange={this.handleChange("passConf")}
              />
            </div>
            {/* <div className={classes.container}>
              <Avatar
                alt="Adelle Charles"
                src={
                  this.state.avatarData
                    ? "http://localhost:2020" +
                      this.state.avatarData.path.slice(7)
                    : ".."
                }
                className={classNames(classes.avatar, classes.bigAvatar)}
              />
              <MyAvatar setData={this.setData} />
            </div> */}
          </div>

          <DialogActions>
            <Button onClick={() => this.props.setAuthorizationFlag("cancel")} color="primary">
              Cancel
            </Button>
            <Button
              onClick={() => this.props.setUserDataSignUp(
                this.props.proxyUser.name,
                this.props.proxyUser.email,
                this.props.proxyUser.password,
                this.props.proxyUser.confpassword,
              )}
              className={classes.button}
              raised
              color="primary"
            >
              send
              {/* <Icon className={classes.rightIcon}>SEND</Icon> */}
            </Button>
            <Button color="primary" onClick={()=> this.props.setAuthorizationFlag("login")}>
                Log in
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state){
  return { 
    user: state.user.user,
    authorization:state.flags.authorization,
    proxyUser: state.proxyUser,
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    setAuthorizationFlag,
    setUserName,
    setUserEmail,
    setUserPassword,
    setUserConfPassword,
    setUserDataSignUp,
    unSetAuthorizationFlag,
    unSetProxyUserData
  }, dispatch);
}

SignUp.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(SignUp));
