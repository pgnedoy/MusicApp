export const styles = theme => ({
  container: {
    // position: "absolute",
    // left: "40%",
    width: "37%",
    // height: 100,
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 300
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing.unit + 6
  },
  rightIcon: {
    left: 100
  },
  background: {
    backgroundColor: "blue",
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  },
  row: {
    marginTop: 20,
    display: "flex",
    width: "100%",
    justifyContent: "center"
  },
  avatar: {
    margin: 10
  },
  bigAvatar: {
    width: 160,
    height: 160
  }
});
