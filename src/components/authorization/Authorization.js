import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { setAuthorizationFlag } from '../../actions/flagsСhange'
import Login from './Login'
import SignUp from './SignUp'
import { Redirect } from "react-router-dom";
import { setUserEmail, setUserPassword, unSetProxyUserData } from '../../actions/setProxyUserData'


const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});


class Authorization extends Component {
  render() {
    const { classes } = this.props;
    if (this.props.authorization === 'login') {
      return <Redirect to="/login" />;
    }else if (this.props.authorization === 'sign-up') {
      this.props.unSetProxyUserData();      
      console.log("!!!");
      return <Redirect to="/sign-up" />;
  
    }else if (this.props.authorization === 'cancel' || this.props.authorization === '' ) {
      return (
        <div>
          <Button className={classes.button} variant="raised" color="secondary" onClick={()=> this.props.setAuthorizationFlag("login")}>
            Login
            <i class="material-icons">exit_to_app</i>
          </Button>
        </div>
      );
    }
  }
}

function mapStateToProps(state){
  return { 
    authorization: state.flags.authorization,
    user:  state.user.user
  }
};
function matchDispatchToProps(dispatch){
  return bindActionCreators({setAuthorizationFlag, unSetProxyUserData}, dispatch);
}


Authorization.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(mapStateToProps, matchDispatchToProps)(Authorization));
