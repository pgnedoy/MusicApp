import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import Avatar from "material-ui/Avatar";
// import UserNameMenu from "./UserNameMenu";

const styles = {
  row: {
    display: "flex",
    justifyContent: "center"
  },
  avatar: {
    margin: 10
  }
};

class UserIcon extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { classes } = this.props;
    return (
      // <h2>!!!!</h2>
      <div className={classes.row}>
        <Avatar
          alt="Remy Sharp"
          src={"..."}
          className={classes.avatar}
        />
        {/* <UserNameMenu {...this.props} /> */}
      </div>
    );
  }
}

UserIcon.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(UserIcon);
