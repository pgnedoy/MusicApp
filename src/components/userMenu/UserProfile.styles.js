export const styles = {
  container: {
    weight: 300
  },
  name: {
    height: 29,
    marginLeft: 10
  },
  email: {
    height: 29,
    marginLeft: 10,
    backgroundColor: "#cfd0d1"
  },
  row: {
    marginTop: 20,
    display: "flex",
    width: "100%",
    justifyContent: "center"
  },
  avatar: {
    margin: 10
  },
  bigAvatar: {
    width: 160,
    height: 160
  }
};
