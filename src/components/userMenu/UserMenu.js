import React from "react";
import Button from "material-ui/Button";
import Menu, { MenuItem } from "material-ui/Menu";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import { withCookies, Cookies } from "react-cookie";
import Icon from "material-ui/Icon";
import { Switch, Route, Link } from "react-router-dom";
import { connect } from 'react-redux'
import UserIcon from './UserIcon'
import { unSetUserData } from '../../actions/setUserData'
import { Redirect } from "react-router";


import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux'
 

const styles = {
  row: {
    display: "flex",
    justifyContent: "center"
  },
  nameButton: {
    top: 12
  },
  icon: {
    color: "disabled",
    margin: "12"
  }
};

class UserMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleLogOut = this.handleLogOut.bind(this);
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = event => {
    this.setState({ anchorEl: null });
  };
  handleLogOut = event => {
    this.props.unSetUserName();
    this.props.unSetEmail();
    this.props.unSetUserId();
    this.props.unSetAvatarData();
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    const { classes } = this.props;
    if (!this.props.user.userId) {
     
      return <Redirect to="/" />;
    }
    return (
      
      // <h2>{this.props.user.userName}</h2>
      <div className={classes.row}>
        <UserIcon />
        
        <Button
          className={classes.nameButton}
          aria-owns={anchorEl ? "simple-menu" : null}
          aria-haspopup="true"
          onClose={this.handleClose}
          onClick={this.handleClick}
        >
          {this.props.user.userName}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <Link to="/profile" style={{ textDecoration: 'none' }}>
            <MenuItem onClick={this.handleClose}>
              <i class="material-icons">account_circle</i>
              My account
            </MenuItem>
          </Link>
          {/* <Link to="/" style={{ textDecoration: 'none' }}> */}
            <MenuItem onClick={() => this.props.unSetUserData()}>
              <Icon color={classes.color}>exit_to_app</Icon>
              Logout
            </MenuItem>
          {/* </Link> */}
        </Menu>
      </div>
    );  
  }
}

function mapStateToProps(state){
  return { 
    user: state.user.user
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    // setAuthorizationFlag,
    // setUserEmail,
    // setUserPassword,
    unSetUserData,
  }, dispatch);
}

UserMenu.propTypes = {
  classes: PropTypes.object.isRequired
};
export default withStyles(styles)(connect(mapStateToProps,matchDispatchToProps)(UserMenu));
