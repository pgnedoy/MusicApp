import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import Avatar from "material-ui/Avatar";
import Button from "material-ui/Button";
import TextField from "material-ui/TextField";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";
import { Redirect } from "react-router-dom";
import config from "../../config.json";
import { styles } from "./UserProfile.styles";
// import logo from "./images.jpeg";
// import MyAvatar from "./Avatar";

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      avatarData: "",
      userName: this.props.userName,
      newUserEmail: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleApply = this.handleApply.bind(this);
    this.rewriteUserName = this.rewriteUserName.bind(this);
    this.setData = this.setData.bind(this);
    this.setAvatarImage = this.setAvatarImage.bind(this);
  }

  handleChange(e) {
    this.setState({ userName: e.target.value });
  }

  setData(data) {
    this.setState({ avatarData: data });
  }

  async rewriteUserName() {
    let user;
    const resp = await fetch(`http://${config.server}:${config.port}/user`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        userId: this.props.userId,
        username: this.state.userName
      })
    });
    try {
      user = await resp.json();
    } catch (error) {
      console.log("error");
      return;
    }
    this.props.unSetUserName();
    this.props.setUserName(user.username);
  }
  async setAvatarImage() {
    let imageData;
    const resp = await fetch(
      `http://${config.server}:${config.port}/avatar/add`,
      {
        headers: {
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify({
          userId: this.props.userId,
          imagePath: this.state.avatarData.path
        })
      }
    );
    try {
      imageData = await resp.json();
    } catch (error) {
      console.log("error");
      return;
    }
    this.props.unSetAvatarData();
    this.props.setAvatarData(imageData.imagepath);
  }
  handleApply() {
    if (
      this.state.userName != "" &&
      this.state.userName != this.props.userName
    ) {
      this.rewriteUserName();
    }
    if (this.state.avatarData) {
      this.setAvatarImage();
    }
  }

  render() {
    const { classes } = this.props;
    let logo = this.props.avatarPath;
    console.log(this.state.avatarData);
    return (
      <div>
        <div className={classes.row}>
          {/* <Avatar
            alt="Adelle Charles"
            src={
              this.state.avatarData
                ? "http://localhost:2020" + this.state.avatarData.path.slice(7)
                : logo
            }
            className={classNames(classes.avatar, classes.bigAvatar)}
          /> */}
        </div>
        <div className={classes.row}>
          {/* <MyAvatar
            {...{
              setData: this.setData,
              setAvatarData: this.props.setAvatarData,
              unSetAvatarData: this.props.unSetAvatarData,
              setError: this.setError
            }}
          /> */}
        </div>
        <div className={classes.row}>
          <p>Name </p>
          <input
            onChange={this.handleChange}
            value={this.state.userName}
            className={classes.name}
          />
        </div>
        <div className={classes.row}>
          <p>Email </p>
          <input
            disabled
            value={this.props.userEmail}
            className={classes.email}
          />
        </div>
        <div className={classes.row}>
          <Button onClick={this.handleApply} color="primary">
            Apply
          </Button>
        </div>
      </div>
    );
  }
}

UserProfile.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(UserProfile);
