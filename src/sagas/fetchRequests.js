import config from "../config.json";


export async function  fetchUserSignUp(props) {
  let resp =  await fetch(`http://${config.server}:${config.port}/user`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({
      email: props.payload.email,
      username: props.payload.name,
      password: props.payload.password,
      passwordConf: props.payload.confpassword 
    })
  })
  try {
    return await resp.json();
  } catch (error) {
    // this.props.setError("Error");
    return;
  }
}

export async function  fetchUserLogin(props) {
  let resp =  await fetch(`http://${config.server}:${config.port}/user`, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    method: "POST",
    body: JSON.stringify({
      logemail: props.payload.email,
      logpassword: props.payload.password
    })
  })
  try {
    return await resp.json();
  } catch (error) {
    // this.props.setError("Error");
    return;
  }
}

export async function getTitles(props) {
  let userId = props.payload.userId;
  const resp = await fetch(
    `http://${config.server}:${config.port}/playlist/titles/${userId}`,
    {
      headers: {
        "Content-Type": "application/json"
      },
      method: "GET"
    }
  );
  try {
    return  await resp.json();
  } catch (error) {
    return;
  }
}

export async function deletePlaylist(props){
  const resp = await fetch(
    `http://${config.server}:${config.port}/playlist/delete-playlist`,
    {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST", 
      body: JSON.stringify({
        userId: props.payload.userId,
        numPly: props.payload.numPly
      })
    }
  );
  try {
    return await resp.json();
  } catch (error) {
    return;
  }
}


export async function getPlaylist(props) {
  let resp = await fetch(
    `http://${config.server}:${config.port}/playlist`,
    {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        userId: props.payload.userId,
        numPly: props.payload.numPly
      })
    }
  );
  try {
    resp = await resp.json();
    return resp.songs;
  } catch (error) {
    // this.props.setError("Error");
    return;
  }
}

export async function deleteSong(props){
  console.log("props", props)
  let resp = await fetch(
    `http://${config.server}:${config.port}/song/delete`,
    {
      headers: {
        "Content-Type": "application/json"
      },
      method: "POST",
      body: JSON.stringify({
        userId: props.userId,
        numPly: props.numPly,
        songIndex: props.index
      })
    }
  );
  try {
    resp = await resp.json();
    return ;
    // await resp.json();
  } catch (error) {
    // this.props.setError("Error");
    return;
  } 
}



// export const updatePlaylist = async list => {
//   let props = this.props.titles;
//   let numPly = props.titles.length - props.currentTitle - 1
//   // let userId = read_cookie("userId");
//   const resp = await fetch(
//     `http://${config.server}:${config.port}/playlist/checkChange`,
//     {
//       headers: {
//         "Content-Type": "application/json"
//       },
//       method: "POST",
//       body: JSON.stringify({
//         userId: userId,
//         numPly: numPly,
//         list: list
//       })
//     }
//   );
//   try {
//     console.log("checkList succesfull");
//   } catch (error) {
//     this.props.setError("Error");
//     return;
//   }
// }