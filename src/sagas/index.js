import { takeLatest, takeEvery, call, put, all, fork } from "redux-saga/effects";
import { connect } from 'react-redux'
import config from "../config.json";
import {  
  API_LOGIN_REQUEST,
  API_LOGIN_SUCCESS,
  API_LOGIN_FAILURE,
  API_SIGNUP_REQUEST,
  API_SIGNUP_SUCCESS,
  API_SIGNUP_FAILURE,
  API_GET_TITLES_REQUEST,
  API_GET_TITLES_SUCCESS,
  API_GET_TITLES_FAILURE,
  API_DELETE_PLAYLIST_REQUEST,
  API_DELETE_PLAYLIST_SUCCESS,
  API_DELETE_PLAYLIST_FAILURE,
  API_GET_PLAYLIST_SUCCESS,
  API_GET_PLAYLIST_FAILURE,
  API_GET_PLAYLIST_REQUEST,
  SET_PLAYLIST,
  API_DELETE_SONG_REQUEST,
  DELETE_SONG, 
  SET_HASH_PLAYLIST,
  API_DELETE_SONG_SUCCESS,
  API_DELETE_SONG_FAILURE,
  API_UPDATE_PLAYLIST_REQUEST,
  API_UPDATE_PLAYLIST_SUCCESS,
  API_UPDATE_PLAYLIST_FAILURE} from "../actions/types";
import { 
  fetchUserLogin, 
  fetchUserSignUp, 
  getTitles, 
  deletePlaylist, 
  getPlaylist,
  deleteSong } from './fetchRequests'
import { setHashPlaylist, setPlaylist, updatePlaylist } from "../actions/setPlaylist";


///////////////////////////////////////////WATCHERS/////////////////////////////
export function* watcherLogin() {
  yield takeLatest(API_LOGIN_REQUEST, workerLogin);
}

export function* watcherSignUp(actions) {
  yield takeLatest(API_SIGNUP_REQUEST, workerSignUp);
}

export function* watcherGetTitles(actions) {
  yield takeLatest(API_GET_TITLES_REQUEST, workerGetTitles);
}

export function* watcherDeletePlaylist(actions) {
  yield takeLatest(API_DELETE_PLAYLIST_REQUEST, workerDeletePlaylist);
}

export function* watcherGetPlaylist(actions) {
  yield takeLatest(API_GET_PLAYLIST_REQUEST, workerGetPlaylist);
}

export function* watcherDeleteSong(actions) {
  yield takeEvery(API_DELETE_SONG_REQUEST, workerDeleteSong);
}

// export function* watcherUpdatePlaylist(actions) {
//   yield takeEvery(API_UPDATE_PLAYLIST_REQUEST, workerUpdatePlaylist);
// }

///////////////////////////////////////////////WORKERS////////////////////////////////////////////////
function* workerSignUp(props) {
  try {
    const user = yield call(fetchUserSignUp, props);
    yield put({ type: API_SIGNUP_SUCCESS, user });
  } catch (error) {
    yield put({ type: API_SIGNUP_FAILURE, error });
  }
}

function* workerLogin(props) {
  try {
    const user = yield call(fetchUserLogin, props);
    yield put({ type: API_LOGIN_SUCCESS, user });
  } catch (error) {
    yield put({ type: API_LOGIN_FAILURE, error });
  }
}

function* workerGetTitles(props) {
  try {
    const titles = yield call(getTitles, props);
    yield put({ type: API_GET_TITLES_SUCCESS, titles });
  } catch (error) {
    yield put({ type: API_GET_TITLES_FAILURE, error });
  }
}

function* workerGetPlaylist(props) {/////will delete playFrom server!!!
  try {
    const playlist = yield call(getPlaylist, props);
    console.log("playlist", playlist)
    const plyForClient = yield call(setPlaylist, playlist);    
    const playlists = {
      playlist,
      plyForClient
    }
    yield put({ type: API_GET_PLAYLIST_SUCCESS, playlists});
  } catch (error) {
    yield put({ type: API_GET_PLAYLIST_FAILURE, error });
  }
}

function* workerDeletePlaylist(props) {
  try {
    const titles = yield call(deletePlaylist, props);
    yield put({ type: API_DELETE_PLAYLIST_SUCCESS, titles });
  } catch (error) {
    yield put({ type: API_DELETE_PLAYLIST_FAILURE, error });
  }
}

function* workerDeleteSong(props) {
  try {
    yield put({ type: API_DELETE_SONG_SUCCESS, props });
    const res = yield call(deleteSong, props.payload);
  } catch (error) {
    yield put({ type: API_DELETE_SONG_FAILURE, error });
  }
}

function* workerUpdatePlaylist(props) {
  try {
    const res = yield call(updatePlaylist, props.payload);
    
    yield put({ type: API_UPDATE_PLAYLIST_SUCCESS, props });
  } catch (error) {
    yield put({ type: API_UPDATE_PLAYLIST_FAILURE, error });
  }
}

export function* rootSaga() {
  yield all([
    fork(watcherSignUp),
    fork(watcherLogin),
    fork(watcherGetTitles),
    fork(watcherGetPlaylist),
    fork(watcherDeletePlaylist),
    fork(watcherDeleteSong), 
    // fork(watcherUpdatePlaylist)
  ])
}