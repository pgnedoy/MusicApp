import React, { Component } from 'react';
import LeftMenu from "./components/LeftMenu";
import Drawer from "material-ui/Drawer";

import MyAppBar from './components/MyAppBar'
import { bindActionCreators } from 'redux';
import { setFalse } from './actions/flagsСhange'
import { connect } from 'react-redux'

// import { Switch, Route, Link } from "react-router";

// import Login from './components/authorization/Login'
// import SignUp from './components/authorization/SignUp'

class MyApp extends Component {
  render() {
    return (
      // <Provider store={store}>
      // <BrowserRouter>

        <div>
          <MyAppBar />
          <Drawer
            open={this.props.leftMenu}
            onClose={() => this.props.setFalse("leftMenu")}
          >
            <div
              tabIndex={0}
              role="button"
              onClick={() => this.props.setFalse("leftMenu")}
              onKeyDown={() => this.props.setFalse("leftMenu")}
            >
              <LeftMenu />
            </div>
          </Drawer>
        </div>
        // </ BrowserRouter>

    );
  }
}

function mapStateToProps(state){
  // console.log(state.flags.leftMenu);
  return { 
    leftMenu: state.flags.leftMenu
  }
};

function matchDispatchToProps(dispatch){
  return bindActionCreators({setFalse: setFalse}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps )(MyApp);
